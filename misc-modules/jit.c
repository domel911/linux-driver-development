/*
 * jit.c -- the just-in-time module
 *
 * Copyright (C) 2001,2003 Alessandro Rubini and Jonathan Corbet
 * Copyright (C) 2001,2003 O'Reilly & Associates
 *
 * The source code in this file can be freely used, adapted,
 * and redistributed in source or binary form, so long as an
 * acknowledgment appears in derived source files.  The citation
 * should list that the code comes from the book "Linux Device
 * Drivers" by Alessandro Rubini and Jonathan Corbet, published
 * by O'Reilly & Associates.   No warranty is attached;
 * we cannot take responsibility for errors or fitness for use.
 *
 * $Id: jit.c,v 1.16 2004/09/26 07:02:43 gregkh Exp $
 */

#include <linux/version.h>
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/init.h>

#include <linux/time.h>
#include <linux/timer.h>
#include <linux/kernel.h>
#include <linux/proc_fs.h>
#include <linux/types.h>
#include <linux/spinlock.h>
#include <linux/interrupt.h>

#include <linux/sched.h>

#include <asm/hardirq.h>
/*
 * This module is a silly one: it only embeds short code fragments
 * that show how time delays can be handled in the kernel.
 */

int delay = HZ; /* the default delay, expressed in jiffies */

module_param(delay, int, 0);

MODULE_AUTHOR("Alessandro Rubini");
MODULE_LICENSE("Dual BSD/GPL");

/* use these as data pointers, to implement four files in one function */
enum jit_files {
	JIT_BUSY,
	JIT_SCHED,
	JIT_QUEUE,
	JIT_SCHEDTO
};

/*
 * This function prints one line of data, after sleeping one second.
 * It can sleep in different ways, according to the data pointer
 */
int jit_fn(char *buf, char **start, off_t offset,
	      int len, int *eof, void *data)
{
	unsigned long j0, j1; /* jiffies */

  //queue of processes waiting for an event
	wait_queue_head_t wait;
	init_waitqueue_head (&wait);
  
  //jiffies is declared as volatile
	j0 = jiffies;
	j1 = j0 + delay;

	switch((long)data) {
    //those _before, _after macros are wrap around safe

    //this method should be definitely avoided, if kernel is configured without preemption
    //then processor is completely locked for the duration of the delay, if interrupts happen
    //to be disabled when entering the loop jiffies won't be updated and condition remains true
    //forever even if preepmption is enabled
		case JIT_BUSY:
			while (time_before(jiffies, j1))
				cpu_relax();
			break;

    //this method releases CPU when it's not currently point of interest, the problem is that even
    //if CPU is released the process remains in the runqueue. If it's the only process in the runqueue
    //it's rescheduled again and again... There is no chance to run IDLE task "swapper" when there is
    //nothing to do and the processor stays all the time under heavy workload. Another disadvantage is
    //that there's no guarantee that the process will get the processor back anytime soon
		case JIT_SCHED:
			while (time_before(jiffies, j1)) {
				schedule();
			}
			break;

    //this method sleeps on given waituqeue, and returns after timeout in jiffies expires
    //function returns 0 on timeout expiration or remaining time if wakeup occurs, returned value
    //is never negative even if delay is greater than expected becouse of system load
		case JIT_QUEUE:
			wait_event_interruptible_timeout(wait, 0, delay);
			break;

    //this method is the same like previous one but avoids waitqueue declaration
		case JIT_SCHEDTO:
      //this line is necessary, the process is not run until scheduler puts it back in TASK_RUNNING state
      //if it wasn't set schedule_timeout behaves like schedule - setting timeout that is not used
			set_current_state(TASK_INTERRUPTIBLE);
			schedule_timeout (delay);
			break;
	}
	j1 = jiffies; /* actual value after we delayed */

	len = sprintf(buf, "%9li %9li\n", j0, j1);
	*start = buf;
	return len;
}

/*
 * This file, on the other hand, returns the current time forever
 */
int jit_currentime(char *buf, char **start, off_t offset,
                   int len, int *eof, void *data)
{
	struct timeval tv1;
	struct timespec tv2;
	unsigned long j1;
	u64 j2;

	/* get them four */
	j1 = jiffies;
  
  //this function does the proper locking when accessing 64bit value on 32bit processor
	j2 = get_jiffies_64();

  //near microsecond resolution
	do_gettimeofday(&tv1);

  //jiffy granurality
	tv2 = current_kernel_time();

	/* print */
	len=0;
	len += sprintf(buf,"0x%08lx 0x%016Lx %10i.%06i\n"
		       "%40i.%09i\n",
		       j1, j2,
		       (int) tv1.tv_sec, (int) tv1.tv_usec,
		       (int) tv2.tv_sec, (int) tv2.tv_nsec);
	*start = buf;
	return len;
}

/*
 * The timer example follows
 */

int tdelay = 10;
module_param(tdelay, int, 0);

/* This data structure used as "data" for the timer and tasklet functions */
struct jit_data {
	struct timer_list timer;
	struct tasklet_struct tlet;
	int hi; /* tasklet or tasklet_hi */
	wait_queue_head_t wait;
	unsigned long prevjiffies;
	unsigned char *buf;
	int loops;
};
#define JIT_ASYNC_LOOPS 5

//Timer function features
//-run as a result of sw interrupt, no process context
//-no access to user space, no path to user space associated with any particular process
//-current(process) is not meaningful
//-no sleeping or scheduling, kmalloc(..., GFP_KERNEL), semaphores - since they can sleep
//-it's possible to reregister self to run again at later time - may be used to implement pooling is run always
//on the same CPU
//-any accessed data should be protected(atomic or spinlock)
//-timer scheduled to be run in the past is scheduled to be run at the next timer tick

//in_interrupt - nonzero if in hw or sw interrupt context
void jit_timer_fn(unsigned long arg)
{
	struct jit_data *data = (struct jit_data *)arg;
	unsigned long j = jiffies;
	data->buf += sprintf(data->buf, "%9li  %3li     %i    %6i   %i   %s\n",
			     j, j - data->prevjiffies, in_interrupt() ? 1 : 0,
			     current->pid, smp_processor_id(), current->comm);

  //reschedule self until loops > 0 then wakeup jit_timer
	if (--data->loops) {
		data->timer.expires += tdelay;
		data->prevjiffies = j;
		add_timer(&data->timer);
	} else {
		wake_up_interruptible(&data->wait);
	}
}

/* the /proc function: allocate everything to allow concurrency */

//read_proc_t parameters:
//-function should start writing at an offset in buf
//-function may write at most len bytes
//-eof should be used to signal EOF has been reached by writing 1 to the memory location eof points to
//-function must return the number of bytes written into the buf
int jit_timer(char *buf, char **start, off_t offset,
	      int len, int *eof, void *unused_data)
{
	struct jit_data *data;
	char *buf2 = buf;
	unsigned long j = jiffies;

	data = kmalloc(sizeof(*data), GFP_KERNEL);
	if (!data)
		return -ENOMEM;

	init_timer(&data->timer);
	init_waitqueue_head (&data->wait);

	/* write the first lines in the buffer */
	buf2 += sprintf(buf2, "   time   delta  inirq    pid   cpu command\n");
	buf2 += sprintf(buf2, "%9li  %3li     %i    %6i   %i   %s\n",
			j, 0L, in_interrupt() ? 1 : 0,
			current->pid, smp_processor_id(), current->comm);

	/* fill the data for our timer function */
	data->prevjiffies = j;
	data->buf = buf2;
	data->loops = JIT_ASYNC_LOOPS;
	
	/* register the timer */
	data->timer.data = (unsigned long)data;
	data->timer.function = jit_timer_fn;
	data->timer.expires = j + tdelay; /* parameter */
	add_timer(&data->timer);

	/* wait for the buffer to fill */
	wait_event_interruptible(data->wait, !data->loops);

  //if awakened by signal - return to the user and let them try again(return appropriate status)
	if (signal_pending(current))
		return -ERESTARTSYS;
	buf2 = data->buf;
	kfree(data);
	*eof = 1;
	return buf2 - buf;
}

//Tasklet function features
//-always run at interrupt time, on the same CPU it was scheduled by
//-can't be asked to be executed at specific time
//-by scheduling it you ask to run it later by the kernel
//-executed in atomic mode in context of soft interrupt(kernel mechanism which executes asynchronous tasks
//with hw interrupts enabled)
//-can be disabled and reenabled later - wont be executed until enabled as many times as disabled
//-can reregister itself
//-can be executed on normal or high priority(high has always precedence over normal)
//-run immediatly(when system not heavy loaded), never later than the next timer tick
//-if rescheduled before has chance to run, it runs only once
//-if rescheduled while it runs - runs again after it completes
//-the execution takes place in the context of ksoftirqd kernel thread(one per CPU)
void jit_tasklet_fn(unsigned long arg)
{
	struct jit_data *data = (struct jit_data *)arg;
	unsigned long j = jiffies;
	data->buf += sprintf(data->buf, "%9li  %3li     %i    %6i   %i   %s\n",
			     j, j - data->prevjiffies, in_interrupt() ? 1 : 0,
			     current->pid, smp_processor_id(), current->comm);

	if (--data->loops) {
		data->prevjiffies = j;
		if (data->hi)
			tasklet_hi_schedule(&data->tlet);
		else
			tasklet_schedule(&data->tlet);
	} else {
		wake_up_interruptible(&data->wait);
	}
}

/* the /proc function: allocate everything to allow concurrency */
int jit_tasklet(char *buf, char **start, off_t offset,
	      int len, int *eof, void *arg)
{
	struct jit_data *data;
	char *buf2 = buf;
	unsigned long j = jiffies;
	long hi = (long)arg;

	data = kmalloc(sizeof(*data), GFP_KERNEL);
	if (!data)
		return -ENOMEM;

	init_waitqueue_head (&data->wait);

	/* write the first lines in the buffer */
	buf2 += sprintf(buf2, "   time   delta  inirq    pid   cpu command\n");
	buf2 += sprintf(buf2, "%9li  %3li     %i    %6i   %i   %s\n",
			j, 0L, in_interrupt() ? 1 : 0,
			current->pid, smp_processor_id(), current->comm);

	/* fill the data for our tasklet function */
	data->prevjiffies = j;
	data->buf = buf2;
	data->loops = JIT_ASYNC_LOOPS;
	
	/* register the tasklet */
	tasklet_init(&data->tlet, jit_tasklet_fn, (unsigned long)data);
	data->hi = hi;
	if (hi)
		tasklet_hi_schedule(&data->tlet);
	else
		tasklet_schedule(&data->tlet);

	/* wait for the buffer to fill */
	wait_event_interruptible(data->wait, !data->loops);

	if (signal_pending(current))
		return -ERESTARTSYS;
	buf2 = data->buf;
	kfree(data);
	*eof = 1;
	return buf2 - buf;
}



int __init jit_init(void)
{
	create_proc_read_entry("currentime", 0, NULL, jit_currentime, NULL);
	create_proc_read_entry("jitbusy", 0, NULL, jit_fn, (void *)JIT_BUSY);
	create_proc_read_entry("jitsched",0, NULL, jit_fn, (void *)JIT_SCHED);
	create_proc_read_entry("jitqueue",0, NULL, jit_fn, (void *)JIT_QUEUE);
	create_proc_read_entry("jitschedto", 0, NULL, jit_fn, (void *)JIT_SCHEDTO);

	create_proc_read_entry("jitimer", 0, NULL, jit_timer, NULL);
	create_proc_read_entry("jitasklet", 0, NULL, jit_tasklet, NULL);
	create_proc_read_entry("jitasklethi", 0, NULL, jit_tasklet, (void *)1);

	return 0; /* success */
}

void __exit jit_cleanup(void)
{
	remove_proc_entry("currentime", NULL);
	remove_proc_entry("jitbusy", NULL);
	remove_proc_entry("jitsched", NULL);
	remove_proc_entry("jitqueue", NULL);
	remove_proc_entry("jitschedto", NULL);

	remove_proc_entry("jitimer", NULL);
	remove_proc_entry("jitasklet", NULL);
	remove_proc_entry("jitasklethi", NULL);
}

module_init(jit_init);
module_exit(jit_cleanup);
